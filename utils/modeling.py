"""
The file contains functions that can help in modeling part (more in jupyter: experiment_0)
author: Rafal B
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# import modeling staff
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn import model_selection
from sklearn.metrics import accuracy_score, roc_auc_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, auc
# skiplot help to plot lift curve
import scikitplot as skplt


def _confusion_score(predict, true):
    cm = confusion_matrix(true, predict)
    return {"TP": cm[0, 0], "FN": cm[0, 1],
            "FN": cm[1, 0], "TN": [cm[1, 1]]}


def make_dataset(df_real: pd.DataFrame, transform: str = None) -> pd.DataFrame:
    """
    transform can be: 'dummy', 'factorize' or None which not create transformation to the data
    """
    df = df_real.copy()
    cat_names = list()
    # change type of columns
    for type_, col in zip(df.dtypes, df.columns):
        if type_ =='object':
            df[col] = df[col].astype('category')
            if transform == 'dummy':
                df = pd.concat([df,pd.get_dummies(df[col], prefix=col)],axis=1)
                df.drop(columns=[col], inplace=True)
            elif transform == 'factorize':
                name = f"{col}_cat"
                df[name] = pd.factorize(df[col])[0]
                df.drop(columns=[col], inplace=True)
            else:
                pass
    return df


# make datasets
def split_data(df: pd.DataFrame, columns: list, transform: str = None) -> dict():
    """Function return dictionary with values for test and 
    campaign groups as {'campaigne': (X, y), 'test': (X, y)}
    """
    # split data into test and campaigne group
    test_group = df[df.test_control_flag != 'campaign group']
    campaign_grop = df[df.test_control_flag == 'campaign group']
    # create x,y dataset
    y_c = campaign_grop.y.eq('yes').mul(1)
    campaign_grop = make_dataset(campaign_grop[columns], transform=transform)
    y_t = test_group.y.eq('yes').mul(1)
    test_group = make_dataset(test_group[columns], transform=transform)
    test_group.fillna(-999, inplace=True)
    data_dict = {'campaign': (campaign_grop, y_c), 'test': (test_group, y_t)}
    return data_dict


def train_validate(train: dict, model_fn, cat_index: list = None, 
                   kfold: int = 10, plot: bool = True):
    """Train model with kfolds. Print metrics and plot
    train: should contains keys: X, y (input data, output variable)
    """
    try: 
        name = type(model_fn).__name__
    except Exception as e:
        name = 'None'
    acc = list()
    roc_auc = list()
    c_matrix = list()
    models = list()
    # make train/test dataset
    X = train['X']
    y = train['y']
    kf = model_selection.KFold(n_splits=kfold, random_state=1, shuffle=True)
    for i, (train_idx, test_idx) in enumerate(kf.split(X)):
        model = model_fn
        y_train, y_valid = y.iloc[train_idx].copy(), y.iloc[test_idx]
        X_train, X_valid = X.iloc[train_idx, :].copy(), X.iloc[test_idx, :].copy()
        if cat_index is not None:
            # works only for catboost
            fit_ = model.fit(X_train, y_train,
                             cat_features=cat_index, verbose=0)
        else:
            fit_ = model.fit(X_train, y_train)
        predict = model.predict(X_valid)
        # get base metrics
        acc.append(accuracy_score(y_valid, predict))
        roc_auc.append(roc_auc_score(y_valid, predict))
        c_matrix.append(_confusion_score(predict, y_valid))
        models.append(model)
    TP = [res['TP'] for res in c_matrix]
    TN = [res['TN'] for res in c_matrix]
    print(f"{name}: {round(np.mean(acc), 3)} accuracy with {round(np.std(acc), 3)} std")
    print(f"{name}: {round(np.mean(roc_auc), 3)} AUC  with {round(np.std(roc_auc), 3)} std")
    if plot is True:
        plt.boxplot(acc)
        plt.ylabel("Accuracy")
        plt.xlabel(name)
        plt.show()
    return models


def predict_model(test, models, roc_auc: bool = True, lift: bool = True):
    """Predict model, print roc auc and litf curves.
    test: dictionary should contain keys: X,y (input test data, output test data)
    """
    proba = list()
    for model in models:
        proba.append(model.predict_proba(test['X']))
    
    pred_val = np.mean(proba, axis=0)
    pred_std = np.std(proba, axis=0)
    if roc_auc is True:
        fpr, tpr, _ = roc_curve(test['y'], pred_val[:, 1])
        roc_auc = auc(fpr, tpr)
        plt.title('Receiver Operating Characteristic')
        plt.plot(fpr, tpr, label='AUC = %0.4f'% roc_auc)
        plt.legend(loc='lower right')
        plt.plot([0,1],[0,1],'r--')
        plt.xlim([-0.001, 1])
        plt.ylim([0, 1.001])
        plt.ylabel('True Positive Rate')
        plt.xlabel('False Positive Rate')
        plt.show()
    if lift is True:
        # Plot the lift curve
        skplt.metrics.plot_lift_curve(test['y'], pred_val)
        plt.show()
    return proba