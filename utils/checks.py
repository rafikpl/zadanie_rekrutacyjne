"""
The file contains functions that can help in checking data sets
author: Rafal B
"""
import pandas as pd
import numpy as np
import seaborn as sns
import warnings
import matplotlib.pyplot as plt

# ignore warnings
warnings.filterwarnings('ignore')

def check_column_status(df: pd.DataFrame) -> None:
    # check columns status and print it out
    for col in df.select_dtypes(include='object').columns:
        try:
            print(f"Unique values for {col} are: {', '.join(df[col].fillna('NAN').unique())}")
            if df[col].isna().sum() > 0:
                  temp = df.dropna().shape[0]
                  print(f"For {col} we have {round(temp/df.shape[0] * 100, 2)}% of NaN values")
                  print("----------------------------")
        except Exception as e:
            print(e, col)
            pass
    pass

def check_group_status(df: pd.DataFrame, control_group: bool = True) -> None:
    # check test and camapign group 
    col_to_check = ['contact', 'month', 'day_of_week', 'pdays']
    if control_group is True:
        group = df[df.test_control_flag != 'campaign group']
        g_name = 'control group'
    else:
        group = df[df.test_control_flag == 'campaign group']
        g_name = 'campaigne group'
    for col in col_to_check:
        try:
            unique_val = '.'.join(group[col].fillna('NAN').unique())
            print(f"Unique values in {g_name} for {col}: {unique_val}")
        except TypeError:
            p_contacted = group[group[col] == 999].shape[0]
            print(f"Numbers of clients which not previously contacted: {p_contacted/group.shape[0] * 100}%")
    # add percentile measure for subscription on deposit
    print("----------------------")
    print(f"The {g_name} subscription term deposit")
    print(group.y.astype('category').value_counts(normalize=True))
    print("----------------------")
    pass
                  
                  
def conversion_rate(data: pd.DataFrame, group: str)-> None:
    # group can be campaign or test
    conversion_rate = (sum(data.y.eq('yes').mul(1))/data.y.count())
    print(f"Conversion rate for {group} is: {round(conversion_rate, 3)*100}%")
    return conversion_rate