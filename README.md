# Zadanie_rekrutacyjne

Predictive task description
You’ve been asked to help the marketing team in increasing performance of their marketing campaign. 
The team wants to laverage Advanced Analytics to improve campaign targeting. In other words they want to identify customers for which the gain from being contacted is the highest. 
To establish a proof of concept they provided data from previous campaign for both control and targeted groups, which were selected at random from non-users before the campaign start.
Aim of the campaign was to persuade customers to subscribe to the term deposit. The product (term deposit) was available also to the control group (but not marketed).

Assuming the total campaign budget is fixed calculate expected lift from using model predictions vs random selection (as done before).

We would like you to laverage relevant analytical methods to develop proof of concept for the next wave of the campaign (same message) and it’s expected lift. For results robustness please propose appropriate evaluation and validation framework.